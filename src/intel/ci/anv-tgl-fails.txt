# 1.3.1.0 uprev:
dEQP-VK.pipeline.extended_dynamic_state.before_good_static.stencil_state_face_front_ge_inc_clamp_clear_255_ref_255_depthfail,Fail
dEQP-VK.pipeline.extended_dynamic_state.between_pipelines.stencil_state_face_front_ge_inc_clamp_clear_255_ref_255_depthfail,Fail

# sharding from 10 to 12 jobs
dEQP-VK.drm_format_modifiers.export_import.r8g8b8a8_unorm,Crash
dEQP-VK.drm_format_modifiers.export_import_fmt_features2.a4r4g4b4_unorm_pack16,Crash
dEQP-VK.drm_format_modifiers.export_import.a8b8g8r8_srgb_pack32,Crash
dEQP-VK.drm_format_modifiers.export_import.r16g16_unorm,Crash

# Fixed upstream with
# 52e160cb81 ("Fix tests that enable depth testing on undefined depth buffer")
# 0f3177eb9e ("Fix tracking image layout in load_store_op_none tests")
dEQP-VK.dynamic_rendering.suballocation.load_store_op_none.stencil_d32_sfloat_s8_uint_load_op_none_store_op_store,Crash
dEQP-VK.dynamic_rendering.suballocation.load_store_op_none.stencil_d24_unorm_s8_uint_load_op_none_store_op_store,Crash
dEQP-VK.dynamic_rendering.suballocation.load_store_op_none.stencil_d32_sfloat_s8_uint_load_op_none_store_op_dontcare,Crash

# New CTS failures in 1.3.5.0
dEQP-VK.drm_format_modifiers.export_import_fmt_features2.b4g4r4a4_unorm_pack16,Crash

# False failure for video formats, which will be fixed once the spec is updated.
# See https://gitlab.khronos.org/Tracker/vk-gl-cts/-/issues/4280
# See https://gitlab.khronos.org/vulkan/vulkan/-/issues/3491
dEQP-VK.api.info.format_properties.g8_b8r8_2plane_420_unorm,Fail
